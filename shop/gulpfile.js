const gulp = require('gulp');
const gulpFn = require('gulp-fn');
const watch = require('gulp-watch');
const batch = require('gulp-batch');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');

const config = require("./gulp-bundle-config/shop.config.json");
createTask(config);

function createTask(bundleConfig) {
    const configCss = require(bundleConfig.bundleFiles.css);
    const configJs = require(bundleConfig.bundleFiles.js);

    gulp.task(bundleConfig.tasks.developerTaskName, function () {
        _compileSass(configCss.bundles, configCss.headers.outputDir);
        _compileJavascript(configJs.bundles, configJs.headers.outputDir);
    });
    gulp.task(bundleConfig.tasks.productionTaskName, function () {
        _compileSassProduction(configCss.bundles, configCss.headers.outputDir);
        _compileJavascriptProduction(configJs.bundles, configJs.headers.outputDir);
    });
    gulp.task(bundleConfig.tasks.watchingTaskName, function () {
        var watchFiles = [].concat(configCss.headers.filesToWatch, configJs.headers.filesToWatch);

        watch(watchFiles, batch(function (events, done) {
            gulp.start(bundleConfig.tasks.developerTaskName, done);
        }));
    });
}

function _compileSass(bundles, outputDir) {
    bundles.forEach(function (bundle) {
        gulp
            .src(bundle.files)
            .pipe(sourcemaps.init())
            .pipe(_sass())
            .pipe(autoprefixer())
            .pipe(concat(bundle.name + ".css"))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(outputDir))
    });
}

function _compileSassProduction(bundles, outputDir) {

    var currentFilePath = "";

    bundles.forEach(function (bundle) {
        gulp
            .src(bundle.files)
            .pipe(gulpFn(function (file) {
                currentFilePath = file.path;
            }))
            .pipe(_sass())
            .pipe(autoprefixer({
                    browsers: ['last 2 versions', 'IE >= 11'],
                    cascade: false
                }
            ))
            .pipe(cleanCSS())
            .pipe(concat(bundle.name + ".css"))
            .pipe(gulp.dest(outputDir))
    });
}

function _compileJavascript(bundles, outputDir) {
    bundles.forEach(function (bundle) {
        gulp.src(bundle.files)
            .pipe(sourcemaps.init())
            .pipe(babel())
            .on('error', console.error.bind(console))
            .pipe(concat(bundle.name + ".js"))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(outputDir))
    });
}

function _compileJavascriptProduction(bundles, outputDir) {
    bundles.forEach(function (bundle) {
        gulp.src(bundle.files)
            .pipe(babel())
            .on('error', console.error.bind(console))
            .pipe(uglify())
            .pipe(concat(bundle.name + ".js"))
            .pipe(gulp.dest(outputDir))
    });
}

function _sass() {
    return sass({includePaths: ["node_modules"]}).on('error', sass.logError);
}

